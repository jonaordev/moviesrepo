package com.selina.selinamovies.movies;

import com.selina.selinamovies.network.entities.Movie;

public interface MoviesActivityInterface {
    void onFragmentCreated();

    void onScrollEnded(int page);

    void onMovieLiked(Movie movie);
}
