package com.selina.selinamovies.movies;

import com.selina.selinamovies.network.entities.Movie;
import com.selina.selinamovies.network.entities.NowPlayingResponse;

import java.util.List;

public interface MoviesContract {
    interface Presenter{

        void requestMovies();

        void requestMovies(int currentPage);

        void onViewCreated(FragmentType fragmentType);

        void onScrollEnded(int page);

        void onDestroy();

        void restoreLastPage(int pageIndex, int pageLastIndex);

        void onMovieLiked(Movie movie);

        void onButtonSwitchClicked();

        int getLastPosition();

        int getPagePosition();
    }
    interface View {

        void onErrorFetchingNetworkData();

        void updateData(NowPlayingResponse response);

        void updateData(List<Movie> movies);
    }
}
