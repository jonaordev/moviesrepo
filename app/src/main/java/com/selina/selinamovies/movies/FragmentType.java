package com.selina.selinamovies.movies;

public enum FragmentType {
    MOVIES_LIST,
    LIKES_LIST
}
