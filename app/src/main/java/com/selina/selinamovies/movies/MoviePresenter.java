package com.selina.selinamovies.movies;

import com.j256.ormlite.dao.Dao;
import com.selina.selinamovies.SelinaMovieApplication;
import com.selina.selinamovies.network.MovieService;
import com.selina.selinamovies.network.entities.Movie;
import com.selina.selinamovies.network.entities.NowPlayingResponse;

import java.sql.SQLException;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MoviePresenter implements MoviesContract.Presenter {

    @Inject
    MovieService mMovieService;
    @Inject
    CompositeDisposable mCompositeDisposable;

    private final Dao<Movie, Integer> mMoviesDao;
    private static int mPageIndex = 1;
    private static int mPageLastIndex = 1;
    private static Integer mCurrentPageIndex;

    private MoviesContract.View mView;

    public MoviePresenter(MoviesContract.View view, Dao moviesDao) {
        mView = view;
        mMoviesDao = moviesDao;
        SelinaMovieApplication.getApplication()
                .getNetworkComponent().inject(this);
    }


    @Override
    public void requestMovies() {
        if (mPageIndex <= mPageLastIndex) {
            mCompositeDisposable.add(mMovieService.service.getNowPlaying(mPageIndex).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

                    .subscribe(new Consumer<NowPlayingResponse>() {
                        @Override
                        public void accept(NowPlayingResponse response) {
                            mView.updateData(response);
                            mPageLastIndex = response.getTotalPages();
                            mCurrentPageIndex = response.getPage();
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            mView.onErrorFetchingNetworkData();
                        }
                    }));
        }
    }

    @Override
    public void requestMovies(int currentPage) {
        if (mCurrentPageIndex == mPageIndex) {
            mPageIndex++;
            requestMovies();
        }
    }

    @Override
    public void onViewCreated(FragmentType fragmentType) {
        switch (fragmentType) {
            case LIKES_LIST: {
                try {
                    mView.updateData(mMoviesDao.queryForAll());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
            default:
            case MOVIES_LIST: {
                requestMovies();
                break;
            }
        }
    }

    @Override
    public void onScrollEnded(int page) {
        requestMovies(page);
    }

    @Override
    public void onDestroy() {
        mCompositeDisposable.clear();
    }

    @Override
    public void onButtonSwitchClicked() {
        mPageIndex = 1;
        mPageLastIndex = 1;
    }

    @Override
    public int getLastPosition() {
        return mPageLastIndex;
    }

    @Override
    public int getPagePosition() {
        return mPageIndex;
    }

    @Override
    public void restoreLastPage(int pageIndex, int pageLastIndex) {
        mPageIndex = pageIndex;
        mPageLastIndex = pageLastIndex;
    }

    @Override
    public void onMovieLiked(Movie movie) {
        try {
            mMoviesDao.createOrUpdate(movie);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
