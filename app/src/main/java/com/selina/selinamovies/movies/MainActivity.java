package com.selina.selinamovies.movies;

import android.app.FragmentManager;
import android.os.Bundle;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.selina.selinamovies.R;
import com.selina.selinamovies.database.DatabaseHelper;
import com.selina.selinamovies.network.entities.Movie;
import com.selina.selinamovies.network.entities.NowPlayingResponse;

import java.sql.SQLException;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends OrmLiteBaseActivity<DatabaseHelper> implements MoviesContract.View, MoviesActivityInterface {

    private static final String CURRENT_LIST_POSITION = "list_position_key";
    private static final String CURRENT_LIST = "list_key";

    private MoviesContract.Presenter mPresenter = null;
    private MoviesFragment mFragment = MoviesFragment.getInstance(
            this, FragmentType.MOVIES_LIST);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            mPresenter = new MoviePresenter(this, getHelper().getDao(Movie.class));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ButterKnife.bind(this);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, mFragment).commit();
    }

    @OnClick(R.id.button)
    public void onButtonClick() {
        switch (mFragment.getFragmentType()) {
            case MOVIES_LIST: {
                mFragment = MoviesFragment.getInstance(this, FragmentType.LIKES_LIST);
                break;
            }
            default:
            case LIKES_LIST: {
                mFragment = MoviesFragment.getInstance(this, FragmentType.MOVIES_LIST);
                break;
            }
        }
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, mFragment).commit();
        mPresenter.onButtonSwitchClicked();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(CURRENT_LIST_POSITION,
                mFragment.getListPosition());
        outState.putParcelableArrayList(CURRENT_LIST, mFragment.getAdapterList());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        mFragment.restoreFragment(savedInstanceState.<Movie>getParcelableArrayList(CURRENT_LIST),
                savedInstanceState.getInt(CURRENT_LIST_POSITION, 0), mPresenter.getPagePosition());
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onErrorFetchingNetworkData() {
        Toast.makeText(this,
                getString(R.string.error_fetching_from_tmdb),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void updateData(NowPlayingResponse response) {
        mFragment.addNewData(response.getMovieList());
    }

    @Override
    public void updateData(List<Movie> movies) {
        mFragment.addNewData(movies);
    }

    @Override
    public void onFragmentCreated() {
        mPresenter.onViewCreated(mFragment.getFragmentType());
    }

    @Override
    public void onScrollEnded(int page) {
        mPresenter.onScrollEnded(page);
    }

    @Override
    public void onMovieLiked(Movie movie) {
        mPresenter.onMovieLiked(movie);
    }
}
