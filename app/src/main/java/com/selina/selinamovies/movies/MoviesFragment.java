package com.selina.selinamovies.movies;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.selina.selinamovies.R;
import com.selina.selinamovies.adapters.RecyclerViewAdapter;
import com.selina.selinamovies.network.entities.Movie;
import com.selina.selinamovies.utils.EndlessScrollListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.recyclerview.animators.SlideInRightAnimator;

public class MoviesFragment extends Fragment {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @Nullable
    private MoviesActivityInterface mInterface;

    private RecyclerViewAdapter mAdapter;
    private EndlessScrollListener mEndlessScrollListener;
    private FragmentType mFragmentType = FragmentType.MOVIES_LIST;


    public static MoviesFragment getInstance(@Nullable MoviesActivityInterface activityInterface,
                                             FragmentType fragmentType) {
        MoviesFragment fragment = new MoviesFragment();
        fragment.mInterface = activityInterface;
        fragment.mFragmentType = fragmentType;
        return fragment;
    }

    public MoviesFragment() {

    }

    public FragmentType getFragmentType() {
        return mFragmentType;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_mvoies,
                container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mAdapter = new RecyclerViewAdapter(getActivity(), new ArrayList<Movie>(), mInterface);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setItemAnimator(new SlideInRightAnimator());
        mRecyclerView.getItemAnimator().setAddDuration(350);

        if (mFragmentType.equals(FragmentType.MOVIES_LIST)) {
            mEndlessScrollListener = new EndlessScrollListener(linearLayoutManager) {
                @Override
                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                    if (mInterface != null) {
                        mInterface.onScrollEnded(page);
                    }
                }
            };
            mRecyclerView.addOnScrollListener(mEndlessScrollListener);
        }

        if (mInterface != null) {
            mInterface.onFragmentCreated();
        }
    }

    public void addNewData(List<Movie> movieList) {
        mAdapter.addNewData(movieList);
    }

    public int getListPosition() {
        return ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                .findFirstCompletelyVisibleItemPosition();
    }

    public ArrayList<? extends Parcelable> getAdapterList() {
        return mAdapter.getMoviesList();
    }

    public void restoreFragment(ArrayList<Movie> movieArrayList, int listPosition, int pagePosition) {
        mAdapter.addNewData(movieArrayList);
        ((LinearLayoutManager) mRecyclerView.getLayoutManager()).scrollToPositionWithOffset(
                listPosition, 0);
    }
}
