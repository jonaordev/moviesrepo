package com.selina.selinamovies;

import android.app.Application;

import com.selina.selinamovies.network.components.DaggerNetworkComponent;
import com.selina.selinamovies.network.components.NetworkComponent;
import com.selina.selinamovies.network.modules.MovieModule;

public class SelinaMovieApplication extends Application {

    private NetworkComponent mNetworkComponent;
    private static SelinaMovieApplication selinaMovieApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        mNetworkComponent = DaggerNetworkComponent.builder()
                .movieModule(new MovieModule())
                .build();

        selinaMovieApplication = this;
    }

    public static SelinaMovieApplication getApplication() {
        return selinaMovieApplication;
    }

    public NetworkComponent getNetworkComponent() {
        return mNetworkComponent;
    }
}
