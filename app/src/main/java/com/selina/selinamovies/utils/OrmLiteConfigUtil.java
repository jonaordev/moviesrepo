package com.selina.selinamovies.utils;

import java.io.IOException;
import java.sql.SQLException;

public class OrmLiteConfigUtil extends com.j256.ormlite.android.apptools.OrmLiteConfigUtil {
    public static void main(String[] args) throws IOException, SQLException {
        writeConfigFile("ormlite_config.txt");
    }
}
