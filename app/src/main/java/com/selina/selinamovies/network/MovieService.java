package com.selina.selinamovies.network;

import com.selina.selinamovies.network.entities.NowPlayingResponse;

import javax.inject.Inject;

import io.reactivex.Single;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class MovieService {
    private static final String NOW_PLAYING_PATH = "movie/now_playing";
    private static final String QUERY_PAGE = "page";

    Retrofit mRetrofit;

    public Service service;

    @Inject
    public MovieService(Retrofit retrofit) {
        mRetrofit = retrofit;
        service = mRetrofit.create(Service.class);
    }

    public interface Service {
        @GET(NOW_PLAYING_PATH)
        Single<NowPlayingResponse> getNowPlaying(@Query(QUERY_PAGE) int pageIndex);
    }

}
