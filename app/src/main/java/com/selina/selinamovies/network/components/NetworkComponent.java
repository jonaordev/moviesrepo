package com.selina.selinamovies.network.components;

import com.selina.selinamovies.movies.MoviePresenter;
import com.selina.selinamovies.network.modules.MovieModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {MovieModule.class})
public interface NetworkComponent {
    void inject(MoviePresenter presenter);
}
