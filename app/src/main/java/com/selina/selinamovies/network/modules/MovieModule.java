package com.selina.selinamovies.network.modules;


import com.selina.selinamovies.network.MovieService;

import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class MovieModule {
    private static final String TMDB_V3_AUTH = "3bfbd3dd4fcffab075f857e806bf5e4d";
    private static final String TMDB_V4_AUTH = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOi" +
            "IzYmZiZDNkZDRmY2ZmYWIwNzVmODU3ZTgwNmJmNWU0ZCIsInN1YiI6IjViOWI1OTBjMGUwYTI2MjhkNzAx" +
            "MTJkMiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.xLx7VnByBbjXKwSpuRQLoCRryMf" +
            "aIktlDGA4jUUDBpY";
    private static final String BASE_URL = "https://api.themoviedb.org/3/";
    private static final String API_KEY_QUERY = "api_key";

    @Provides
    @Singleton
    Retrofit provideRetrofit() {
        //interceptor is used to add auth to every network call
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        HttpUrl url = request.url().newBuilder().addQueryParameter(API_KEY_QUERY,
                                TMDB_V3_AUTH).build();
                        request = request.newBuilder().url(url).build();
                        return chain.proceed(request);
                    }
                })
                .build();

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    MovieService provideMovieService(Retrofit retrofit) {
        return new MovieService(retrofit);
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }
}
