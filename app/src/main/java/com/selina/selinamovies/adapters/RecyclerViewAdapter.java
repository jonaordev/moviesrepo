package com.selina.selinamovies.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.selina.selinamovies.movies.MoviesActivityInterface;
import com.selina.selinamovies.R;
import com.selina.selinamovies.network.entities.Movie;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MovieViewHolder> {

    private final MoviesActivityInterface mInterface;
    private ArrayList<Movie> movies;
    private Context mContext;

    public RecyclerViewAdapter(Context context, ArrayList<Movie> movies,
                               MoviesActivityInterface moviesInterface) {
        mInterface = moviesInterface;
        mContext = context;
        this.movies = movies;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_list_item,
                parent, false);
        return new MovieViewHolder(rootView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        final Movie movie = movies.get(position);
        holder.movieTitle.setText(movie.getTitle());
        holder.movieDesc.setText(movie.getOverview());
        holder.ratingText.setText(movie.getVoteAverage().toString());
        String posterPath = movies.get(position).getPosterPath();
        Picasso.get().load("http://image.tmdb.org/t/p/w500" +
                posterPath).into(holder.image);
        holder.likeButton.setOnClickListener(new View.OnClickListener() {
            ;

            @Override
            public void onClick(View view) {
                mInterface.onMovieLiked(movie);
                Toast.makeText(mContext, R.string.added_fabs, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public void addNewData(List<Movie> newData) {
        int tempIndex = movies.size();
        movies.addAll(newData);
        notifyItemRangeChanged(tempIndex, newData.size());
    }

    public ArrayList<Movie> getMoviesList() {
        return movies;
    }

    class MovieViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.movieTitle)
        AppCompatTextView movieTitle;
        @BindView(R.id.movieDesc)
        AppCompatTextView movieDesc;
        @BindView(R.id.image)
        AppCompatImageView image;
        @BindView(R.id.ratingText)
        AppCompatTextView ratingText;
        @BindView(R.id.likeButton)
        AppCompatImageView likeButton;

        MovieViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
